Using the Web Interface
=======================
The first step in integrating your ground station is to get accustomed to the web interface. 
The web platform can be accessed at https://timgsn.informatik.uni-wuerzburg.de. Please contact
us directly to obtain your login and password details. The most important details of using the
web interface are listed below.

Projects and Workspaces
-----------------------

Making Requests
---------------

Accepting Requests
------------------

Uploading Data
--------------

Downloading Data
----------------

Visualizing Data
----------------