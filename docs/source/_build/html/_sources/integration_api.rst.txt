Using the TIMGSN REST API
=========================
The REST API allows all of the operations possible on the web application in an automated manner. Therefore, you can 
implement the common operations and act upon them through your existing software infrastructure. All you need is to 
make HTTP requests to our API, using your login credentials and project details. We outline them below in Python.
There are three main APIs that we provide, namely, OverpassAPI, RequestAPI, and DataAPI. Let us take a look at them individually.

Overpass API
------------

* Provides a list of overpasses for a given satellite, station and time period.
* Endpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses

[GET] Find Overpasses
^^^^^^^^^^^^^^^^^^^^^
Use the GET HTTP Method to get a list of overpasses. 

.. code-block:: Python
   :linenos:

   import requests
   import datetime

   ## endpoint
   URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses"

   ## credentials
   TEST_USER_CREDENTIALS = (username, password) # use your username and password
   
   ## request data
   norad = 39446 # satellite NORAD ID
   station_name = 'siset_uvhf' # see manual for list of station names
   start_time = datetime.datetime.now() # all times are in UTC
   end_time = start_time + datetime.timedelta(days=7)

   # date format must be YYYY-mm-dd HH:MM:SS
   start_time_str = start_time.strftime("%Y-%m-%d %H:%M:%S")
   end_time_str = end_time.strftime("%Y-%m-%d %H:%M:%S")
   data = {
       'norad': norad,
       'station_name': station_name,
       'start_time': start_time_str,
       'end_time': end_time_str
   }

   ## make a GET request to API
   response = requests.get(url=URL, auth=TEST_USER_CREDENTIALS, data=data)
   
   ## get the json response
   res_json = response.json() # see manual for json structure
   overpasses = res_json['overpasses']
   uids = [op["uid"] for op in overpasses]
   

Requests API
------------

* Facilitates retrieving requests and posting requests
* Endpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests

[POST] Making Requests
^^^^^^^^^^^^^^^^^^^^^^
In order to make a request, the operator must select an overpass. The `UID` of the overpass is needed to request it. See code below.
Please note that you can only make requests for satellites that you own.

.. code-block:: Python
   :linenos:

   import requests

   ## endpoint
   URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

   ## credentials
   TEST_USER_CREDENTIALS = (username, password) # use your username and password

   ## request data
   project_name = 'test_project' # create a project from web application
   request_type = "TELEMETRY" # see manual for allowed request types
   overpass_uid = 'jmuw_uhf-39446--2020-11-17--16' # obtained from Overpass API

   response = requests.post(url=URL,
                            auth=TEST_USER_CREDENTIALS,
                            data={
                                'project_name': 'test_project',
                                'overpass_uid': overpass_uid,
                                'request_type': request_type
                            })
    res_json = response.json() # see manual for json structure
    tracking_request = res_json.get('requests')[0] if 'requests' in res_json


[GET] Query All Sent Requests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows you to find which requests were sent by you.


.. code-block:: Python
   :linenos:

   import requests

   ## endpoint
   URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

   ## credentials
   TEST_USER_CREDENTIALS = (username, password) # use your username and password

   ## request data
   direction = 'sent' # See Manual

   response = requests.get(url=URL,
                           auth=TEST_USER_CREDENTIALS,
                           data={'direction': direction})
   res_json = response.json() # see manual for json structure
   sent_requests = res_json['requests'] if 'requests' in sent_requests


[GET] Query All Received Requests
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows you to find which requests were received by you.


.. code-block:: Python
   :linenos:

   import requests

   ## endpoint
   URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

   ## credentials
   TEST_USER_CREDENTIALS = (username, password) # use your username and password

   ## request data
   direction = 'received' # See Manual

   response = requests.get(url=URL,
                           auth=TEST_USER_CREDENTIALS,
                           data={'direction': direction})
   res_json = response.json() # see manual for json structure
   sent_requests = res_json['requests'] if 'requests' in sent_requests


Request API
------------
* Facilitates retrieving and modifying a request
* Endpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/<request_uid>
* Please note the endpoint is ``request`` and not ``requests``

[GET] Query Details of a Valid Request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows you to query details of a particular request

.. code-block:: Python
   :linenos:

   import requests

   ## valid request uid
   request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

   ## endpoint
   URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

   ## credentials
   TEST_USER_CREDENTIALS = (username, password) # use your username and password
   
   ## make get request
   response = requests.get(URL, auth=TEST_USER_CREDENTIALS)

   ## request data is sent back by server
   tracking_request = response.json() # see manual to parse tracking request


[DELETE] Delete a Valid Request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Allows you to delete a particular request.

.. code-block:: Python
    :linenos:

    import requests
   
    ## valid request uid
    request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

    ## endpoint
    URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

    ## credentials
    TEST_USER_CREDENTIALS = (username, password) # use your username and password
    
    ## make get request
    response = requests.delete(URL, auth=TEST_USER_CREDENTIALS)

    ## request data is sent back by server
    res_json = response.json() # see manual 
    
    deleted = res_json['result'] # True if deleted else False


[PUT] Update Status of A Valid Request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
You can update the status of a valid request, e.g., mark it as success, failure, etc. See manual for all possible values.

.. code-block:: Python
    :linenos:

    import requests
   
    ## valid request uid
    request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

    ## endpoint
    URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

    ## credentials
    TEST_USER_CREDENTIALS = (username, password) # use your username and password
    
    ## data
    action = 'accept' # actions can be: accept, reject, tracked, or failed
    data = {'action': action}

    ## accept one of the tracking requests you received
    response = requests.put(URL,
                            auth=TEST_USER_CREDENTIALS,
                            data=data)
    ## request data is sent back by server
    res_json = response.json() # see manual to parse json


Telemetry API
--------------
* Facilitates upload and download of data
* Endpoint: https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry/<overpass_uid>
* Since data is shared with all users, the Data API works on overpasses rather than tracking request as input.


[GET] Download Packet Data for an Overpass
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Retrieve packet data for an overpass. The data is zipped and must be unzipped for further use.

.. code-block:: Python
    :linenos:

    from io import BytesIO
    import zipfile
    import requests

    ## valid overpass UID
    overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

    ## endpoint
    URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

    ## credentials
    TEST_USER_CREDENTIALS = (username, password) # use your username and password

    ## data

    # data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
    packet_data_format = 'DEMODULATED' # fetch demodulated data

    # output of the API response can be json or raw
    response_output =  'raw' # get raw bytes as output
    
    data = {'format': packet_data_format, 
            'output': response_output}
    response = requests.get(URL,
                            auth=TEST_USER_CREDENTIALS,
                            data=data,
                            stream=True)
    memory_file = BytesIO(response.content)

    # see manual for more details
    with zipfile.ZipFile(memory_file, 'r', zipfile.ZIP_DEFLATED) as zf:
        print(f'number of files are: {len(zf.infolist())}')
        for name in zf.namelist():
            data = zf.read(name)
            print(f"name: {name} \t length: {len(data)} \n")
            print('-'*30)
            print('\n')

            

[POST] Upload Packet Data for an Overpass
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Upload packet data for an overpass. 

.. code-block:: Python
    :linenos:

    from io import BytesIO
    import zipfile
    import requests

    
    ## valid overpass UID
    overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

    ## endpoint
    URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

    ## credentials
    TEST_USER_CREDENTIALS = (username, password) # use your username and password

    ## packet bytes
    packet = bytearray([0xce]) # use your packet data
    bytes_ = bytes(packet)
    overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

    ## data

    # data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
    data = {'format': 'DEMODULATED'} 
    
    # send bytes as file
    files = {'file': BytesIO(bytes_)}

    response = requests.post(URL,
                             auth=TEST_USER_CREDENTIALS,
                             data=data,
                             files=files)
    uploaded = response['result'] if 'result' in response # True if success else False