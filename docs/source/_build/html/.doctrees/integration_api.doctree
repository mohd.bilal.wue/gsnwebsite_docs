��=q      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Using the TIMGSN REST API�h]�h	�Text����Using the TIMGSN REST API�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�xc:\Users\mob33uk\Desktop\ProjectWork\TOM\VSCodeWorkspace\gsnwebsite_bp_integral\app_flex\docs\source\integration_api.rst�hKubh	�	paragraph���)��}�(hX�  The REST API allows all of the operations possible on the web application in an automated manner. Therefore, you can
implement the common operations and act upon them through your existing software infrastructure. All you need is to
make HTTP requests to our API, using your login credentials and project details. We outline them below in Python.
There are three main APIs that we provide, namely, OverpassAPI, RequestAPI, and DataAPI. Let us take a look at them individually.�h]�hX�  The REST API allows all of the operations possible on the web application in an automated manner. Therefore, you can
implement the common operations and act upon them through your existing software infrastructure. All you need is to
make HTTP requests to our API, using your login credentials and project details. We outline them below in Python.
There are three main APIs that we provide, namely, OverpassAPI, RequestAPI, and DataAPI. Let us take a look at them individually.�����}�(hh0hh.hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhhhubh)��}�(hhh]�(h)��}�(h�Overpass API�h]�h�Overpass API�����}�(hhAhh?hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh<hhhh+hK	ubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h�MProvides a list of overpasses for a given satellite, station and time period.�h]�h-)��}�(hhVh]�h�MProvides a list of overpasses for a given satellite, station and time period.�����}�(hhVhhXubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhTubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhhOhhhh+hNubhS)��}�(h�HEndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses
�h]�h-)��}�(h�GEndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses�h]�(h�
Endpoint: �����}�(h�
Endpoint: �hhoubh	�	reference���)��}�(h�=https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses�h]�h�=https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses�����}�(hhhhzubah}�(h ]�h"]�h$]�h&]�h(]��refuri�h|uh*hxhhoubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhhkubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhhOhhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]��bullet��*�uh*hMhh+hKhh<hhubh)��}�(hhh]�(h)��}�(h�[GET] Find Overpasses�h]�h�[GET] Find Overpasses�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh�hhhh+hKubh-)��}�(h�4Use the GET HTTP Method to get a list of overpasses.�h]�h�4Use the GET HTTP Method to get a list of overpasses.�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKhh�hhubh	�literal_block���)��}�(hX�  import requests
import datetime

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
norad = 39446 # satellite NORAD ID
station_name = 'siset_uvhf' # see manual for list of station names
start_time = datetime.datetime.now() # all times are in UTC
end_time = start_time + datetime.timedelta(days=7)

# date format must be YYYY-mm-dd HH:MM:SS
start_time_str = start_time.strftime("%Y-%m-%d %H:%M:%S")
end_time_str = end_time.strftime("%Y-%m-%d %H:%M:%S")
data = {
    'norad': norad,
    'station_name': station_name,
    'start_time': start_time_str,
    'end_time': end_time_str
}

## make a GET request to API
response = requests.get(url=URL, auth=TEST_USER_CREDENTIALS, data=data)

## get the json response
res_json = response.json() # see manual for json structure
overpasses = res_json['overpasses']
uids = [op["uid"] for op in overpasses]�h]�hX�  import requests
import datetime

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/overpasses"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
norad = 39446 # satellite NORAD ID
station_name = 'siset_uvhf' # see manual for list of station names
start_time = datetime.datetime.now() # all times are in UTC
end_time = start_time + datetime.timedelta(days=7)

# date format must be YYYY-mm-dd HH:MM:SS
start_time_str = start_time.strftime("%Y-%m-%d %H:%M:%S")
end_time_str = end_time.strftime("%Y-%m-%d %H:%M:%S")
data = {
    'norad': norad,
    'station_name': station_name,
    'start_time': start_time_str,
    'end_time': end_time_str
}

## make a GET request to API
response = requests.get(url=URL, auth=TEST_USER_CREDENTIALS, data=data)

## get the json response
res_json = response.json() # see manual for json structure
overpasses = res_json['overpasses']
uids = [op["uid"] for op in overpasses]�����}�(hhhh�ubah}�(h ]�h"]�h$]�h&]�h(]��	xml:space��preserve��linenos���force���language��Python��highlight_args�}�uh*h�hh+hKhh�hhubeh}�(h ]��get-find-overpasses�ah"]�h$]��[get] find overpasses�ah&]�h(]�uh*h
hh<hhhh+hKubeh}�(h ]��overpass-api�ah"]�h$]��overpass api�ah&]�h(]�uh*h
hhhhhh+hK	ubh)��}�(hhh]�(h)��}�(h�Requests API�h]�h�Requests API�����}�(hh�hh�hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhh�hhhh+hK8ubhN)��}�(hhh]�(hS)��}�(h�4Facilitates retrieving requests and posting requests�h]�h-)��}�(hh�h]�h�4Facilitates retrieving requests and posting requests�����}�(hh�hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK:hh�ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhh�hhhh+hNubhS)��}�(h�FEndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests
�h]�h-)��}�(h�EEndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests�h]�(h�
Endpoint: �����}�(h�
Endpoint: �hj  ubhy)��}�(h�;https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests�h]�h�;https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]��refuri�j  uh*hxhj  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK;hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhh�hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*hMhh+hK:hh�hhubh)��}�(hhh]�(h)��}�(h�[POST] Making Requests�h]�h�[POST] Making Requests�����}�(hjB  hj@  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj=  hhhh+hK>ubh-)��}�(h��In order to make a request, the operator must select an overpass. The `UID` of the overpass is needed to request it. See code below.
Please note that you can only make requests for satellites that you own.�h]�(h�FIn order to make a request, the operator must select an overpass. The �����}�(h�FIn order to make a request, the operator must select an overpass. The �hjN  hhhNhNubh	�title_reference���)��}�(h�`UID`�h]�h�UID�����}�(hhhjY  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jW  hjN  ubh�� of the overpass is needed to request it. See code below.
Please note that you can only make requests for satellites that you own.�����}�(h�� of the overpass is needed to request it. See code below.
Please note that you can only make requests for satellites that you own.�hjN  hhhNhNubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK?hj=  hhubh�)��}�(hXp  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
project_name = 'test_project' # create a project from web application
request_type = "TELEMETRY" # see manual for allowed request types
overpass_uid = 'jmuw_uhf-39446--2020-11-17--16' # obtained from Overpass API

response = requests.post(url=URL,
                         auth=TEST_USER_CREDENTIALS,
                         data={
                             'project_name': 'test_project',
                             'overpass_uid': overpass_uid,
                             'request_type': request_type
                         })
 res_json = response.json() # see manual for json structure
 tracking_request = res_json.get('requests')[0] if 'requests' in res_json�h]�hXp  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
project_name = 'test_project' # create a project from web application
request_type = "TELEMETRY" # see manual for allowed request types
overpass_uid = 'jmuw_uhf-39446--2020-11-17--16' # obtained from Overpass API

response = requests.post(url=URL,
                         auth=TEST_USER_CREDENTIALS,
                         data={
                             'project_name': 'test_project',
                             'overpass_uid': overpass_uid,
                             'request_type': request_type
                         })
 res_json = response.json() # see manual for json structure
 tracking_request = res_json.get('requests')[0] if 'requests' in res_json�����}�(hhhjr  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hKBhj=  hhubeh}�(h ]��post-making-requests�ah"]�h$]��[post] making requests�ah&]�h(]�uh*h
hh�hhhh+hK>ubh)��}�(hhh]�(h)��}�(h�[GET] Query All Sent Requests�h]�h�[GET] Query All Sent Requests�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK^ubh-)��}�(h�3Allows you to find which requests were sent by you.�h]�h�3Allows you to find which requests were sent by you.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK_hj�  hhubh�)��}�(hX�  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
direction = 'sent' # See Manual

response = requests.get(url=URL,
                        auth=TEST_USER_CREDENTIALS,
                        data={'direction': direction})
res_json = response.json() # see manual for json structure
sent_requests = res_json['requests'] if 'requests' in sent_requests�h]�hX�  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
direction = 'sent' # See Manual

response = requests.get(url=URL,
                        auth=TEST_USER_CREDENTIALS,
                        data={'direction': direction})
res_json = response.json() # see manual for json structure
sent_requests = res_json['requests'] if 'requests' in sent_requests�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hKbhj�  hhubeh}�(h ]��get-query-all-sent-requests�ah"]�h$]��[get] query all sent requests�ah&]�h(]�uh*h
hh�hhhh+hK^ubh)��}�(hhh]�(h)��}�(h�![GET] Query All Received Requests�h]�h�![GET] Query All Received Requests�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hKxubh-)��}�(h�7Allows you to find which requests were received by you.�h]�h�7Allows you to find which requests were received by you.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hKyhj�  hhubh�)��}�(hX�  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
direction = 'received' # See Manual

response = requests.get(url=URL,
                        auth=TEST_USER_CREDENTIALS,
                        data={'direction': direction})
res_json = response.json() # see manual for json structure
sent_requests = res_json['requests'] if 'requests' in sent_requests�h]�hX�  import requests

## endpoint
URL = "https://timgsn.informatik.uni-wuerzburg.de/api/ops/requests"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## request data
direction = 'received' # See Manual

response = requests.get(url=URL,
                        auth=TEST_USER_CREDENTIALS,
                        data={'direction': direction})
res_json = response.json() # see manual for json structure
sent_requests = res_json['requests'] if 'requests' in sent_requests�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hK|hj�  hhubeh}�(h ]��get-query-all-received-requests�ah"]�h$]��![get] query all received requests�ah&]�h(]�uh*h
hh�hhhh+hKxubeh}�(h ]��requests-api�ah"]�h$]��requests api�ah&]�h(]�uh*h
hhhhhh+hK8ubh)��}�(hhh]�(h)��}�(h�Request API�h]�h�Request API�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj   hhhh+hK�ubhN)��}�(hhh]�(hS)��}�(h�.Facilitates retrieving and modifying a request�h]�h-)��}�(hj  h]�h�.Facilitates retrieving and modifying a request�����}�(hj  hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhj  hhhh+hNubhS)��}�(h�REndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/<request_uid>�h]�h-)��}�(hj-  h]�(h�
Endpoint: �����}�(h�
Endpoint: �hj/  ubhy)��}�(h�:https://timgsn.informatik.uni-wuerzburg.de/api/ops/request�h]�h�:https://timgsn.informatik.uni-wuerzburg.de/api/ops/request�����}�(hhhj7  ubah}�(h ]�h"]�h$]�h&]�h(]��refuri�j9  uh*hxhj/  ubh�/<request_uid>�����}�(h�/<request_uid>�hj/  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj+  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhj  hhhh+hNubhS)��}�(h�=Please note the endpoint is ``request`` and not ``requests``
�h]�h-)��}�(h�<Please note the endpoint is ``request`` and not ``requests``�h]�(h�Please note the endpoint is �����}�(h�Please note the endpoint is �hj[  ubh	�literal���)��}�(h�``request``�h]�h�request�����}�(hhhjf  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jd  hj[  ubh�	 and not �����}�(h�	 and not �hj[  ubje  )��}�(h�``requests``�h]�h�requests�����}�(hhhjy  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*jd  hj[  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hjW  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhj  hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*hMhh+hK�hj   hhubh)��}�(hhh]�(h)��}�(h�&[GET] Query Details of a Valid Request�h]�h�&[GET] Query Details of a Valid Request�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK�ubh-)��}�(h�3Allows you to query details of a particular request�h]�h�3Allows you to query details of a particular request�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj�  hhubh�)��}�(hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## make get request
response = requests.get(URL, auth=TEST_USER_CREDENTIALS)

## request data is sent back by server
tracking_request = response.json() # see manual to parse tracking request�h]�hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## make get request
response = requests.get(URL, auth=TEST_USER_CREDENTIALS)

## request data is sent back by server
tracking_request = response.json() # see manual to parse tracking request�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hK�hj�  hhubeh}�(h ]��$get-query-details-of-a-valid-request�ah"]�h$]��&[get] query details of a valid request�ah&]�h(]�uh*h
hj   hhhh+hK�ubh)��}�(hhh]�(h)��}�(h�[DELETE] Delete a Valid Request�h]�h�[DELETE] Delete a Valid Request�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK�ubh-)��}�(h�*Allows you to delete a particular request.�h]�h�*Allows you to delete a particular request.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj�  hhubh�)��}�(hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## make get request
response = requests.delete(URL, auth=TEST_USER_CREDENTIALS)

## request data is sent back by server
res_json = response.json() # see manual

deleted = res_json['result'] # True if deleted else False�h]�hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## make get request
response = requests.delete(URL, auth=TEST_USER_CREDENTIALS)

## request data is sent back by server
res_json = response.json() # see manual

deleted = res_json['result'] # True if deleted else False�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hK�hj�  hhubeh}�(h ]��delete-delete-a-valid-request�ah"]�h$]��[delete] delete a valid request�ah&]�h(]�uh*h
hj   hhhh+hK�ubh)��}�(hhh]�(h)��}�(h�&[PUT] Update Status of A Valid Request�h]�h�&[PUT] Update Status of A Valid Request�����}�(hj  hj
  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj  hhhh+hK�ubh-)��}�(h�yYou can update the status of a valid request, e.g., mark it as success, failure, etc. See manual for all possible values.�h]�h�yYou can update the status of a valid request, e.g., mark it as success, failure, etc. See manual for all possible values.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj  hhubh�)��}�(hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## data
action = 'accept' # actions can be: accept, reject, tracked, or failed
data = {'action': action}

## accept one of the tracking requests you received
response = requests.put(URL,
                        auth=TEST_USER_CREDENTIALS,
                        data=data)
## request data is sent back by server
res_json = response.json() # see manual to parse json�h]�hX�  import requests

## valid request uid
request_uid = "6ea6a1fb-efd0-4b0c-8622-54d6f074b6ef"

## endpoint
URL = f"https://timgsn.informatik.uni-wuerzburg.de/api/ops/request/{request_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## data
action = 'accept' # actions can be: accept, reject, tracked, or failed
data = {'action': action}

## accept one of the tracking requests you received
response = requests.put(URL,
                        auth=TEST_USER_CREDENTIALS,
                        data=data)
## request data is sent back by server
res_json = response.json() # see manual to parse json�����}�(hhhj&  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hK�hj  hhubeh}�(h ]��$put-update-status-of-a-valid-request�ah"]�h$]��&[put] update status of a valid request�ah&]�h(]�uh*h
hj   hhhh+hK�ubeh}�(h ]��request-api�ah"]�h$]��request api�ah&]�h(]�uh*h
hhhhhh+hK�ubh)��}�(hhh]�(h)��}�(h�Telemetry API�h]�h�Telemetry API�����}�(hjK  hjI  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhjF  hhhh+hK�ubhN)��}�(hhh]�(hS)��}�(h�'Facilitates upload and download of data�h]�h-)��}�(hj\  h]�h�'Facilitates upload and download of data�����}�(hj\  hj^  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hjZ  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhjW  hhhh+hNubhS)��}�(h�VEndpoint: https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry/<overpass_uid>�h]�h-)��}�(hjs  h]�(h�
Endpoint: �����}�(h�
Endpoint: �hju  ubhy)��}�(h�=https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry�h]�h�=https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry�����}�(hhhj}  ubah}�(h ]�h"]�h$]�h&]�h(]��refuri�j  uh*hxhju  ubh�/<overpass_uid>�����}�(h�/<overpass_uid>�hju  ubeh}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hjq  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhjW  hhhh+hNubhS)��}�(h�nSince data is shared with all users, the Data API works on overpasses rather than tracking request as input.

�h]�h-)��}�(h�lSince data is shared with all users, the Data API works on overpasses rather than tracking request as input.�h]�h�lSince data is shared with all users, the Data API works on overpasses rather than tracking request as input.�����}�(hj�  hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj�  ubah}�(h ]�h"]�h$]�h&]�h(]�uh*hRhjW  hhhh+hNubeh}�(h ]�h"]�h$]�h&]�h(]�h�h�uh*hMhh+hK�hjF  hhubh)��}�(hhh]�(h)��}�(h�*[GET] Download Packet Data for an Overpass�h]�h�*[GET] Download Packet Data for an Overpass�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hK�ubh-)��}�(h�^Retrieve packet data for an overpass. The data is zipped and must be unzipped for further use.�h]�h�^Retrieve packet data for an overpass. The data is zipped and must be unzipped for further use.�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hK�hj�  hhubh�)��}�(hXp  from io import BytesIO
import zipfile
import requests

## valid overpass UID
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## endpoint
URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## data

# data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
packet_data_format = 'DEMODULATED' # fetch demodulated data

# output of the API response can be json or raw
response_output =  'raw' # get raw bytes as output

data = {'format': packet_data_format,
        'output': response_output}
response = requests.get(URL,
                        auth=TEST_USER_CREDENTIALS,
                        data=data,
                        stream=True)
memory_file = BytesIO(response.content)

# see manual for more details
with zipfile.ZipFile(memory_file, 'r', zipfile.ZIP_DEFLATED) as zf:
    print(f'number of files are: {len(zf.infolist())}')
    for name in zf.namelist():
        data = zf.read(name)
        print(f"name: {name} \t length: {len(data)} \n")
        print('-'*30)
        print('\n')�h]�hXp  from io import BytesIO
import zipfile
import requests

## valid overpass UID
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## endpoint
URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## data

# data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
packet_data_format = 'DEMODULATED' # fetch demodulated data

# output of the API response can be json or raw
response_output =  'raw' # get raw bytes as output

data = {'format': packet_data_format,
        'output': response_output}
response = requests.get(URL,
                        auth=TEST_USER_CREDENTIALS,
                        data=data,
                        stream=True)
memory_file = BytesIO(response.content)

# see manual for more details
with zipfile.ZipFile(memory_file, 'r', zipfile.ZIP_DEFLATED) as zf:
    print(f'number of files are: {len(zf.infolist())}')
    for name in zf.namelist():
        data = zf.read(name)
        print(f"name: {name} \t length: {len(data)} \n")
        print('-'*30)
        print('\n')�����}�(hhhj�  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hK�hj�  hhubeh}�(h ]��(get-download-packet-data-for-an-overpass�ah"]�h$]��*[get] download packet data for an overpass�ah&]�h(]�uh*h
hjF  hhhh+hK�ubh)��}�(hhh]�(h)��}�(h�)[POST] Upload Packet Data for an Overpass�h]�h�)[POST] Upload Packet Data for an Overpass�����}�(hj�  hj�  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*hhj�  hhhh+hM ubh-)��}�(h�#Upload packet data for an overpass.�h]�h�#Upload packet data for an overpass.�����}�(hj  hj  hhhNhNubah}�(h ]�h"]�h$]�h&]�h(]�uh*h,hh+hM!hj�  hhubh�)��}�(hX`  from io import BytesIO
import zipfile
import requests


## valid overpass UID
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## endpoint
URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## packet bytes
packet = bytearray([0xce]) # use your packet data
bytes_ = bytes(packet)
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## data

# data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
data = {'format': 'DEMODULATED'}

# send bytes as file
files = {'file': BytesIO(bytes_)}

response = requests.post(URL,
                         auth=TEST_USER_CREDENTIALS,
                         data=data,
                         files=files)
uploaded = response['result'] if 'result' in response # True if success else False�h]�hX`  from io import BytesIO
import zipfile
import requests


## valid overpass UID
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## endpoint
URL = f"{https://timgsn.informatik.uni-wuerzburg.de/api/data/telemetry}/{overpass_uid}"

## credentials
TEST_USER_CREDENTIALS = (username, password) # use your username and password

## packet bytes
packet = bytearray([0xce]) # use your packet data
bytes_ = bytes(packet)
overpass_uid = 'jmuw_uhf-39446--2021-03-01--13'

## data

# data format: possible values are 'IQ', 'DEMODULATED', 'DECODED'
data = {'format': 'DEMODULATED'}

# send bytes as file
files = {'file': BytesIO(bytes_)}

response = requests.post(URL,
                         auth=TEST_USER_CREDENTIALS,
                         data=data,
                         files=files)
uploaded = response['result'] if 'result' in response # True if success else False�����}�(hhhj  ubah}�(h ]�h"]�h$]�h&]�h(]�h�h�hΈhωhЌPython�h�}�uh*h�hh+hM#hj�  hhubeh}�(h ]��'post-upload-packet-data-for-an-overpass�ah"]�h$]��)[post] upload packet data for an overpass�ah&]�h(]�uh*h
hjF  hhhh+hM ubeh}�(h ]��telemetry-api�ah"]�h$]��telemetry api�ah&]�h(]�uh*h
hhhhhh+hK�ubeh}�(h ]��using-the-timgsn-rest-api�ah"]�h$]��using the timgsn rest api�ah&]�h(]�uh*h
hhhhhh+hKubah}�(h ]�h"]�h$]�h&]�h(]��source�h+uh*h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j\  �error_encoding��cp1252��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h+�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j6  j3  h�h�h�h�j�  j�  j�  j�  j�  j�  j�  j�  jC  j@  j�  j�  j  j  j;  j8  j.  j+  j�  j�  j&  j#  u�	nametypes�}�(j6  Nh�Nh�Nj�  Nj�  Nj�  Nj�  NjC  Nj�  Nj  Nj;  Nj.  Nj�  Nj&  Nuh }�(j3  hh�h<h�h�j�  h�j�  j=  j�  j�  j�  j�  j@  j   j�  j�  j  j�  j8  j  j+  jF  j�  j�  j#  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.