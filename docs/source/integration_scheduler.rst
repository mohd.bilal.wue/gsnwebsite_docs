Using the Scheduler
===================
The following steps shall be followed to integrate your ground station within the TIMGSN.

Install AWSIoTSDK
-----------------
AWS IoT Core is used to provide MQTT and IoT services to the ground stations. Install a version of the `AWSIoTSDK`_ which 
is available for Python, JavaScript, Java, and C++. Select a language of your choice depending on existing software
architecture on your ground station. Below we show the Python version.::
    
    pip install awsiotsdk==1.4.9


Download AWS Certificates & Keys
--------------------------------
To start using AWS IoT, you need to download AWS Root certificate, private and public keys from the `settings page`_ of your TIMGSN
Website Account.


.. _AWSIoTSDK: https://docs.aws.amazon.com/iot/latest/developerguide/iot-sdks.html

.. _settings page: https://timgsn.informatik.uni-wuerzburg.de/auth/settings/security

Connect with AWS IoT
--------------------
With the downloaded certificates and keys, you can now connect to our AWS IoT endpoint. The endpoint is shared with you with the login details.

.. code-block:: python
   :linenos:

   from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient as AWS
   import os

   aws = AWS(os.getenv('AWS_ID'))
   aws.configureEndpoint(os.getenv('AWS_ENDPOINT'), 8883)
   aws.configureCredentials(os.getenv('AWS_ROOT_CERT'),
                            os.getenv('AWS_PRIVATE_KEY'),
                            os.getenv('AWS_CERT'))
    aws.connect()

Receive Schedules
-----------------
Once connected to the AWS IoT Client, you can subscribe to the topics for receiving regular schedule updates and automate your operations.
Below is a code snippet to do this in Python.

.. code-block:: python
   :linenos:

   import json

   def callback(client, user_data, message):
       data = json.loads(message.payload)
       overpasses = data.get("overpasses")
       scheduling_method = data.get("scheduling_method")
       updated_on = data.get('created_on')

       # here implement code to act on the schedules for your station
       # e.g.
       my_station_name = 'wurzburg_uhf'
       my_schedule = [op for op in overpasses if op['station_name']==my_station_name]
       # pass my_schedule to existing system

   topic = 'schedule'
   QoS = 1
   aws.subscribe(topic, QoS, callback)
