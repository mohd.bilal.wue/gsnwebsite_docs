TIMGSN Concept
==============
The TIMGSN aims to fulfil two major tasks in context of creating a global network
of ground stations. The first aim is to increase visibility of satellites and the 
second is to share data and therefore leverage data fusion techniques. 

To achieve these aims, we provide a way to optimally schedule overpasses for the 
ground stations on the network using sophisticated real time scheduling algorithms 
and provide a user friendly interface to operators for creating requests and visualizing
shared data. 

All this is achieved by the three components of the TIMGSN as described below. 

* **TIMGSN Web Application**: Provide interface to make requests for overpasses and visualize data.
* **TIMGSN REST API**: Facilitates automation of operational tasks like requesting and implementing schedule
* **TIMGSN Scheduler**: Computes optimal schedule and published results over MQTT.

Therefore, participants of the network benefit from increased satellite visibility and reduced
ground station downtimes. If this gets you interested, go ahead and read how to integrate your
existing setup into our network.
