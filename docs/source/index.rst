.. TIMGSN documentation master file, created by
   sphinx-quickstart on Mon Mar 22 16:07:15 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

TIMGSN Documentation
====================
This is the documentation of the Telematics International Mission Ground Station Network (TIMGSN),
a collaborative network of ground stations for optimal scheduling and sharing data.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   concept
   integration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
