Format for Telemetry Decoders
=============================
As new data arrives, TIMGSN backend will decode demodulated data, store it and update the dashboards of the concerned satellite.
In order to decode the demodulated data, we need to have an implementation of the satellite decoders. You can share the decoders
with us as Python code. An example decoder is shown below. 

The return value of the decoder should be a list of measurements as in the
example decoder. You can also share the details of your packet data and we can implement the python code for you.

.. code-block:: Python
   :linenos:

   
   from utils.signal_processing.protocols.ax25 import *

   # the ax25 package defines AX25 Packets

   def decoder_ax25(data, *args, **kwargs):
        packet = from_bytes(bytearray(data))
        res = [
            {
                "measurement": "temperature",
                "tags": {
                    "sender": packet.src.callsign,
                    "norad": 39446,
                    "recipient": packet.dest.callsign,
                    "units": "Celcius"
                },
                "fields":{
                    "value": 160.0
                },
                "time": time.time_ns()
            }
        ]
        return res

    